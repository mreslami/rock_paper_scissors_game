import 'package:flutter/animation.dart';
import 'package:get/get.dart';
import '../pages/game/binding/game_binding.dart';
import '../pages/game/screen/game_screen.dart';
import '../pages/splash/splash.dart';
import '../pages/splash/splash_binding.dart';



class AppRouter {
  static List<GetPage<dynamic>>? getPages() {
    return [

      GetPage(
        name: "/",
        page: () => const Splash(),
        binding: SplashBinding(),
      ),
      GetPage(
          name: "/game",
          page: () =>  GameScreen(),
          binding:GameBinding()
      ),
    ];
  }
}
