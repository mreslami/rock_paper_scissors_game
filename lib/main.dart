import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'core/router.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Rock Paper Scissors',
      initialRoute: "/",
      getPages: AppRouter.getPages(),
    );
  }
}