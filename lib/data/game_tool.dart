import 'dart:ui';

import '../core/tool_type.dart';

class GameTool {
  final ToolType type;
  Offset position;
  Offset velocity;

  GameTool(this.type, this.position, this.velocity);
}