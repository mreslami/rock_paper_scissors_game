import 'dart:async';
import 'dart:ui';

import 'package:get/get.dart';


class SplashController extends GetxController implements GetxService {



  @override
  void onInit() {
    super.onInit();
    Timer(const Duration(seconds: 2), () {
      Get.offAndToNamed("/game");
    });
  }




}