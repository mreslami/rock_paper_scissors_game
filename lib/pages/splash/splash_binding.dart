import 'package:get/get.dart';
import 'package:rock_paper_scissors_game/pages/splash/splash_controller.dart';


class SplashBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(SplashController());
  }
}
