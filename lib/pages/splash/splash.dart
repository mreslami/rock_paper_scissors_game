import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rock_paper_scissors_game/pages/splash/splash_controller.dart';

class Splash extends GetView<SplashController> {
  const Splash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(41, 143, 254, 1.0),
      body: SafeArea(
        child: Center(child: Image.asset("assets/images/splash.png",height: Get.height*0.1,)),
      ),
    );
  }
}
