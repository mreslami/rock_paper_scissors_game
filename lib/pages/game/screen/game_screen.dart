import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controller/game_controller.dart';

class GameScreen extends StatelessWidget {
  final GameController controller = Get.put(GameController());

   GameScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color:  const Color.fromRGBO(41, 143, 254, 1.0),
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: const Text('Rock Paper Scissors'),
          ),
          body: Container(
            color: Colors.white,
            child:GetBuilder<GameController>(
              init: GameController(),
              builder: (controller) => Stack(
                children: [
                  // Render game objects
                  for (var object in controller.tools)
                    Positioned(
                      left: object.position.dx,
                      top: object.position.dy,
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            color: controller.getToolColor(object.type),

                            shape: BoxShape.circle
                        ),
                        width: 50,
                        height: 50,
                        child: Center(
                          child: Image.asset(
                           controller.getToolImage(object.type),
                          ),
                        ),
                      ),
                    ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
