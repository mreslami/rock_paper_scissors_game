import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/tool_type.dart';
import '../../../data/game_tool.dart';


class GameController extends GetxController {
  final List<GameTool> tools = [];

  @override
  void onInit() {
    super.onInit();
    for (int i = 0; i < 5; i++) {
      tools.add(GameTool(ToolType.rock, getRandomPosition(), getRandomVelocity()));
      tools.add(GameTool(ToolType.paper, getRandomPosition(), getRandomVelocity()));
      tools.add(GameTool(ToolType.scissors, getRandomPosition(), getRandomVelocity()));
    }
    updateGame();
  }

  void updateGame() {
    Timer.periodic(const Duration(milliseconds: 16), (t) {
      List<GameTool> toolsToRemove = [];

      for (int i = 0; i < tools.length; i++) {
        GameTool tool = tools[i];

        tool.position += tool.velocity;

        if (tool.position.dx < 0 || tool.position.dx > Get.width - 50) {
          tool.velocity = Offset(-tool.velocity.dx, tool.velocity.dy);
        }
        if (tool.position.dy < 0 || tool.position.dy > Get.height - 130) {
          tool.velocity = Offset(tool.velocity.dx, -tool.velocity.dy);
        }

        for (int j = i + 1; j < tools.length; j++) {
          GameTool other = tools[j];

          if (tool.position.dx < other.position.dx + 50 &&
              tool.position.dx + 50 > other.position.dx &&
              tool.position.dy < other.position.dy + 50 &&
              tool.position.dy + 50 > other.position.dy) {
            if (tool.type == other.type) {
              tool.velocity = Offset(-tool.velocity.dx, -tool.velocity.dy);
              other.velocity=getRandomVelocity();
            } else {
              if ((tool.type == ToolType.rock && other.type == ToolType.scissors) ||
                  (tool.type == ToolType.paper && other.type == ToolType.rock) ||
                  (tool.type == ToolType.scissors && other.type == ToolType.paper)) {
                toolsToRemove.add(other);
              } else {
                toolsToRemove.add(tool);
                break;
              }
            }
          }
        }
      }

      // Remove tools that need to be removed
      for (GameTool toolToRemove in toolsToRemove) {
        tools.remove(toolToRemove);
      }

      update();
    });
  }

  Offset getRandomPosition() {
    final random = Random();
    final screenWidth = Get.width;
    final screenHeight = Get.height;
    return Offset(random.nextDouble() * (screenWidth - 50), random.nextDouble() * (screenHeight - 50));
  }

  Offset getRandomVelocity() {
    final random = Random();
    return Offset(random.nextDouble() * 4 - 2, random.nextDouble() * 4 - 2);
  }

  Color getToolColor(ToolType type) {
    switch (type) {
      case ToolType.rock:
        return Colors.black;
      case ToolType.paper:
        return Colors.blue;
      case ToolType.scissors:
        return Colors.red;
    }
  }

  String getToolImage(ToolType type) {
    switch (type) {
      case ToolType.rock:
        return 'assets/images/rock.png';
      case ToolType.paper:
        return 'assets/images/paper.png';
      case ToolType.scissors:
        return 'assets/images/scissors.png';
    }
  }
}
